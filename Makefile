#### Toolchain commands ####
GCC_INCLUDE_DIR		:= /usr/arm-none-eabi/include/
GCC_PREFIX          := arm-none-eabi

CC      := $(GCC_PREFIX)-gcc
AS      := $(GCC_PREFIX)-as
AR      := $(GCC_PREFIX)-ar -r
LD      := $(GCC_PREFIX)-ld
NM      := $(GCC_PREFIX)-nm
OBJDUMP := $(GCC_PREFIX)-objdump
OBJCOPY := $(GCC_PREFIX)-objcopy
GDB     := $(GCC_PREFIX)-gdb
SIZE    := $(GCC_PREFIX)-size

MK	:= mkdir -p
RM	:= rm -rf

#nRF52 CFlag defines
NO_NFC_REQD     := CONFIG_NFCT_PINS_AS_GPIOS
PINRESET_REQD   := CONFIG_GPIO_AS_PINRESET

### General Variables ####
OUTPUT_NAME		= main
OUTPUT_DIR		= build
OBJ_DIR			= obj
SRC_DIR			= src
LINK_DIR		= link

### Device related stuff ###
BOARD			:= BLUEY_1V1
CPU				:= cortex-m4
DEVICE 			:= NRF52
DEVICESERIES 	:= nrf52

### Programmer ###
JLINK_DIR 			= /opt/SEGGER/JLink/
JLINK 				= $(JLINK_DIR)JLinkExe
JLINKGDBSERVER		= $(JLINK_DIR)JLinkGDBServer
JLINKDEVICE 		= nrf52
NVMC_CONFIG_ADRS	= 4001E504

FLASH_START_ADDRESS	= 0
#SOFTDEVICE 			= {path to the softdevice}.hex

# Include directories
INCLUDEDIRS	 = $(shell find include -type d)
INCLUDEDIRS	+= $(GCC_INCLUDE_DIR)
### Source files ###
# Project Source
C_SRC  = $(shell find src -name *.c | awk -F/ '{print $$NF}')

### Assembly source files
ASSEMBLY_SRC = gcc_startup_$(DEVICESERIES).S

### Verbosity control. Use  make V=1  to get verbose builds.
ifeq ($(V),1)
  Q=
else
  Q=@
endif

### Compiler related stuff ###
#Small size
CFLAGS	= -Os
#info for the debugger
CFLAGS  += -ggdb
CFLAGS	+= -mcpu=$(CPU)
CFLAGS	+= -mthumb
CFLAGS	+= -mabi=aapcs
CFLAGS  += -mfloat-abi=hard #Which floating point ABI to use
CFLAGS  += -mfpu=fpv4-sp-d16 #The type of FPU we are using
#Use the GCC extensions to C
CFLAGS	+= --std=gnu11
#Give a warning when using the GCC extensions to C
CFLAGS  += -pedantic
CFLAGS	+= -Wall -Werror
#CFLAGS  += -ffunction-sections      #Create a separate function section
#CFLAGS  += -fdata-sections          #Create a separate data section
CFLAGS	+= -D$(DEVICE)
CFLAGS	+= -D$(BOARD)
CFLAGS  += -DDEBUG
CFLAGS  += -D$(NO_NFC_REQD)
CFLAGS  += -D$(PINRESET_REQD)
CFLAGS  += -DNRF52_PAN_64
CFLAGS  += -DNRF52_PAN_12
CFLAGS  += -DNRF52_PAN_58
CFLAGS  += -DNRF52_PAN_54
CFLAGS  += -DNRF52_PAN_31
CFLAGS  += -DNRF52_PAN_51
CFLAGS  += -DNRF52_PAN_36
CFLAGS  += -DNRF52_PAN_15
CFLAGS  += -DNRF52_PAN_20
CFLAGS  += -DNRF52_PAN_55
CFLAGS	+= $(patsubst %,-I%, $(INCLUDEDIRS))

### Linker related stuff ###
LDDIRS		+= $(LINK_DIR)

LD_SCRIPT = $(LINK_DIR)/nrf52_xxaa.ld

LDFLAGS = -Xlinker
LDFLAGS += -Map=$(OUTPUT_DIR)/$(OUTPUT_NAME).map
LDFLAGS += -mcpu=$(CPU)
# use newlib in nano version
LDFLAGS += --specs=nano.specs
LDFLAGS += -mthumb 
LDFLAGS += -mabi=aapcs
LDFLAGS  += -mfloat-abi=hard #Which floating point ABI to use
LDFLAGS  += -mfpu=fpv4-sp-d16 #The type of FPU we are using 
# let linker to dump unused sections
LDFLAGS += -Wl,--gc-sections -flto
LDFLAGS += -T$(LD_SCRIPT)
LDFLAGS	+= -D$(DEVICE)
LDFLAGS	+= -D$(BOARD)
LDFLAGS	+= $(patsubst %,-L%, $(LDDIRS))

# Sorting removes duplicates
BUILD_DIRS := $(sort $(OBJ_DIR) $(OUTPUT_DIR) )

# Make a list of source paths
C_SRC_DIRS = $(shell find src -type d) 
ASSEMBLY_SRC_DIRS = $(shell find src -type d)

# Object files
C_OBJ 			= $(addprefix $(OBJ_DIR)/, $(C_SRC:.c=.o))
ASSEMBLY_OBJ 	= $(addprefix $(OBJ_DIR)/, $(ASSEMBLY_SRC:.S=.o))

# Set source lookup paths
vpath %.c $(C_SRC_DIRS)
vpath %.S $(ASSEMBLY_SRC_DIRS)

# Include automatically previously generated dependencies
-include $(addprefix $(OBJ_DIR)/, $(C_OBJ:.o=.d))

### Rules ###
# Default build target
.PHONY : all size clean

size :
	$(SIZE) $(OUTPUT_DIR)/$(OUTPUT_NAME).elf

all : release

clean : 
	$(RM) $(OUTPUT_DIR)/*
	$(RM) $(OBJ_DIR)/*
	@echo

.PHONY: release
release :  $(OUTPUT_DIR)/$(OUTPUT_NAME).bin $(OUTPUT_DIR)/$(OUTPUT_NAME).hex

$(BUILD_DIRS) : 
	@echo 
	@echo "Creating directories"
	- $(MK) $@

# Create objects from C source files
$(OBJ_DIR)/%.o : %.c
	@echo "CC " $<
	$(Q)$(CC) $(CFLAGS) -M $< -MF "$(@:.o=.d)" -MT $@	
	$(Q)$(CC) $(CFLAGS) -c -o $@ $<

## Assemble .S files
$(OBJ_DIR)/%.o : %.S
	@echo "CC " $<
	$(Q)$(CC) $(CFLAGS) -c -o $@ $<

## Link C and assembler objects to an .elf file
$(OUTPUT_DIR)/$(OUTPUT_NAME).elf : $(BUILD_DIRS) $(C_OBJ) $(ASSEMBLY_OBJ)
	@echo
	@echo "LD $(OUTPUT_DIR)/$(OUTPUT_NAME).elf" 
	$(Q)$(CC) $(LDFLAGS) $(C_OBJ) $(ASSEMBLY_OBJ) -o $(OUTPUT_DIR)/$(OUTPUT_NAME).elf

## Create binary .bin file from the .elf file
$(OUTPUT_DIR)/$(OUTPUT_NAME).bin : $(OUTPUT_DIR)/$(OUTPUT_NAME).elf
	@echo
	@echo "Create" $@ "file from" $<
	$(Q)$(OBJCOPY) -O binary $(OUTPUT_DIR)/$(OUTPUT_NAME).elf $(OUTPUT_DIR)/$(OUTPUT_NAME).bin

## Create binary .hex file from the .elf file
$(OUTPUT_DIR)/$(OUTPUT_NAME).hex : $(OUTPUT_DIR)/$(OUTPUT_NAME).elf
	@echo "Create" $@ "file from" $<
	$(Q)$(OBJCOPY) -O ihex $(OUTPUT_DIR)/$(OUTPUT_NAME).elf $(OUTPUT_DIR)/$(OUTPUT_NAME).hex

## Program device
upload:
	$(GDB) -q -ex 'target extended-remote /dev/ttyBmpGdb' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'load' -ex 'compare-sections' -ex 'detach' -ex 'quit' $(OUTPUT_DIR)/$(OUTPUT_NAME).hex; \

eraseall:
	$(GDB) -q -ex 'target extended-remote /dev/ttyBmpGdb' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'mon erase_mass' -ex 'detach' -ex 'quit'; \

reset:
	$(GDB) --batch -q -ex 'target extended-remote /dev/ttyBmpGdb' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'kill'; \

debug:
	$(GDB) -q -ex 'set confirm no' -ex 'target extended-remote /dev/ttyBmpGdb' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'load' -ex 'run' $(OUTPUT_DIR)/$(OUTPUT_NAME).elf; \

.PHONY: upload eraseall reset debug
